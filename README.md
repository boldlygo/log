# `boldlygo/log` - a simple leveled, structured log

`boldlygo/log` provides a simple leveled, structured log.

## Example

```go
package main

import (
	"context"
	"net"
	"net/http"
	"os"

	"gitlab.com/boldlygo/log"
)

type Handler struct {
	log.Logger
}

func (h Handler) handle(w http.ResponseWriter, r *http.Request) {
	// Log with the fields in r.Context() and level debug.
	h.Log(r.Context(), log.LevelDebug)
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Include the request URI in any log entries.
	ctx := log.Context(r.Context(), log.NewField("uri", r.RequestURI))
	r = r.WithContext(ctx)
	h.handle(w, r)
}

func main() {
	logger := log.New(os.Stderr)

	svr := &http.Server{
		Addr: ":http",
		Handler: Handler{
			Logger: logger,
		},
		ErrorLog:    logger.Logger,
		BaseContext: func(net.Listener) context.Context {
			return log.Context(context.Background(), log.NewField("name", "value"))
		},
	}

	svr.ListenAndServe()
}
```