package log

import (
	"fmt"
	"io"
	"io/ioutil"
	"time"
)

var _ io.Writer = entry{}

type entry struct {
	*Log

	time   time.Time
	level  Level
	msg    Msg
	fields map[string]interface{}
}

func (e *entry) setLevel(v interface{}) bool {
	switch x := v.(type) {
	case Level:
		e.level = x
		return true

	case string:
		if lvl, err := ParseLevel(x); err == nil {
			e.level = lvl
			return true
		}
	}

	return false
}

func (e *entry) setMsg(v interface{}) bool {
	switch x := v.(type) {
	case Msg:
		e.msg = x
		return true
	case string:
		e.msg = Msg(x)
		return true
	}

	return false
}

func (e *entry) setTime(v interface{}) bool {
	if t, ok := v.(time.Time); ok {
		e.time = t
		return true
	}

	return false
}

func (e *entry) setField(k string, v interface{}) {
	var ok bool

	switch k {
	case FieldKeyLevel:
		ok = e.setLevel(v)
	case FieldKeyMessage:
		ok = e.setMsg(v)
	case FieldKeyTime:
		ok = e.setTime(v)
	}

	if ok {
		return
	}

	if x, ok := v.(error); ok {
		v = x.Error()
	}

	e.fields[k] = v
}

func (e entry) Write(p []byte) (n int, err error) {
	if e.level < e.minLevel {
		return ioutil.Discard.Write(p)
	}

	if len(p) > 0 {
		e.fields[FieldKeyMessage] = Msg(fmt.Sprintf("%s", p))
	}

	f := e.f
	if f == nil {
		f = TextFormat{}
	}

	bs, err := f.Format(e)
	if err != nil {
		return
	}

	n, err = e.w.Write(append(bs, '\n'))

	switch {
	// case false && e.Level == LevelError:
	// 	os.Exit(1)
	// case false && e.Level == LevelPanic:
	// 	panic(s)
	}

	return n, err
}
