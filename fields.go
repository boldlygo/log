package log

import "fmt"

// Wrap returns a Field for the provided key, value pair.
func Wrap(key string, value interface{}) Field {
	switch key {
	case FieldKeyLevel:
		if lvl, ok := value.(Level); ok {
			return lvl
		}

		return field{key, value}

	case FieldKeyMessage:
		if msg, ok := value.(Msg); ok {
			return msg
		}

		if msg, ok := value.(string); ok {
			return Msg(msg)
		}

		return field{key, value}

	default:
		return field{key, value}
	}
}

var (
	_ Field = field{}
	_ Field = Msg("")
)

type field struct {
	key   string
	value interface{}
}

func (f field) Key() string { return f.key }

func (f field) Get() interface{} { return f.value }

// Msg is a log message.
type Msg string

// Msgf formats according to a format specifier and returns the resulting Msg.
func Msgf(format string, a ...interface{}) Msg {
	return Msg(fmt.Sprintf(format, a...))
}

// Get implements the Field interface.
func (m Msg) Get() interface{} { return m }

// Key implements the Field interface.
func (Msg) Key() string { return FieldKeyMessage }
