//nolint:funlen,goerr113,lll // tests
package log_test

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"

	"gitlab.com/boldlygo/log"
)

func TestLog_Log(t *testing.T) {
	now := time.Now()

	type fields struct {
		w   *bytes.Buffer
		lvl log.Level
		f   log.Formatter
	}

	type args struct {
		ctx    context.Context
		fields []log.Field
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			"JSON",
			fields{&bytes.Buffer{}, log.LevelInfo, log.JSONFormat{}},
			args{
				context.Background(),
				[]log.Field{
					log.Wrap(log.FieldKeyTime, now),
				},
			},
			fmt.Sprintf("{\"time\":\"%s\",\"level\":\"%s\"}\n", now.Format(time.RFC3339Nano), log.LevelInfo),
		},
		{
			"JSON",
			fields{&bytes.Buffer{}, log.LevelInfo, log.JSONFormat{}},
			args{
				log.NewContext(log.NewContext(context.Background(), log.Msg("message")), log.Wrap("key", "value")),
				[]log.Field{
					log.Wrap(log.FieldKeyTime, now),
					log.LevelError,
					log.Wrap(log.FieldKeyError, errors.New("an error")),
				},
			},
			fmt.Sprintf("{\"time\":\"%s\",\"level\":\"%s\",\"msg\":\"message\",\"error\":\"an error\",\"key\":\"value\"}\n", now.Format(time.RFC3339Nano), log.LevelError),
		},
		{
			"text",
			fields{&bytes.Buffer{}, log.LevelInfo, log.TextFormat{}},
			args{
				context.Background(),
				[]log.Field{
					log.Wrap(log.FieldKeyTime, now),
				},
			},
			fmt.Sprintf("\"time\"=\"%s\" \"level\"=\"%s\"\n", now.Format(time.RFC3339Nano), log.LevelInfo),
		},
		{
			"text",
			fields{&bytes.Buffer{}, log.LevelInfo, log.TextFormat{}},
			args{
				log.NewContext(log.NewContext(context.Background(), log.Msg("message")), log.Wrap("key", "value")),
				[]log.Field{
					log.Wrap(log.FieldKeyTime, now),
					log.LevelError,
					log.Wrap(log.FieldKeyError, errors.New("an error")),
				},
			},
			fmt.Sprintf("\"time\"=\"%s\" \"level\"=\"%s\" \"msg\"=\"message\" \"error\"=\"an error\" \"key\"=\"value\"\n", now.Format(time.RFC3339Nano), log.LevelError),
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			l := log.New(tt.fields.w, tt.fields.lvl, tt.fields.f)
			l.Log(tt.args.ctx, tt.args.fields...)
			if diff := cmp.Diff(tt.want, tt.fields.w.String()); diff != "" {
				t.Errorf("log.Log() output mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
