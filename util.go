package log

func merge(a, b []Field) []Field {
	c := make([]Field, len(a)+len(b))

	copy(c, a)
	copy(c[len(a):], b)

	return c
}
