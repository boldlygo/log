package log

import (
	"encoding/json"
	"time"
)

var (
	_ Formatter      = JSONFormat{}
	_ json.Marshaler = entry{}
)

// JSONFormat formats log entries as JSON.
type JSONFormat struct{}

// Format implements the Formatter interface.
func (JSONFormat) Format(e entry) ([]byte, error) {
	return e.MarshalJSON()
}

func (e entry) MarshalJSON() ([]byte, error) {
	v := struct {
		Time  time.Time `json:"time"`
		Level Level     `json:"level"`
		Msg   Msg       `json:"msg,omitempty"`
	}{e.time, e.level, e.msg}

	text, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	if len(e.fields) == 0 {
		return text, nil
	}

	fields, err := json.Marshal(e.fields)
	if err != nil {
		return nil, err
	}

	text = append(text[:len(text)-1], ',')
	text = append(text, fields[1:]...)

	return text, nil
}
