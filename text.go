package log

import (
	"bytes"
	"encoding"
	"fmt"
	"sort"
	"time"
)

var (
	_ Formatter              = TextFormat{}
	_ encoding.TextMarshaler = entry{}
)

// TextFormat formats log entries as plain text.
type TextFormat struct{}

// Format implements the Formatter interface.
func (TextFormat) Format(e entry) ([]byte, error) {
	return e.MarshalText()
}

func (e entry) MarshalText() ([]byte, error) {
	buf := &bytes.Buffer{}
	fmt.Fprintf(buf, "%q=%q ", FieldKeyTime, e.time.Format(time.RFC3339Nano))
	fmt.Fprintf(buf, "%q=%q ", FieldKeyLevel, e.level)

	if msg := e.msg; msg != "" {
		fmt.Fprintf(buf, "%q=%q ", FieldKeyMessage, msg)
	}

	keys := make(sort.StringSlice, 0, len(e.fields))
	for k := range e.fields {
		keys = append(keys, k)
	}

	keys.Sort()

	for _, k := range keys {
		fmt.Fprintf(buf, "%q=%q ", k, e.fields[k])
	}

	buf.Truncate(buf.Len() - 1)

	return buf.Bytes(), nil
}
