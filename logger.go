package log

import (
	"context"
)

// Common field keys.
const (
	FieldKeyError   = "error"
	FieldKeyLevel   = "level"
	FieldKeyMessage = "msg"
	FieldKeyTime    = "time"
)

// Field is the interface implemented by types that store structured data for logging.
type Field interface {
	Key() string
	Get() interface{}
}

// Logger is the interface implemented by types that can write to a log.
type Logger interface {
	Log(context.Context, ...Field)
}

type ctxKey struct{}

// NewContext returns a copy of ctx in which the provided Fields, if any, are added to the set of Fields stored in ctx.
func NewContext(ctx context.Context, field ...Field) context.Context {
	return context.WithValue(ctx, ctxKey{}, merge(FromContext(ctx), field))
}

// FromContext returns the fields stored in ctx.
func FromContext(ctx context.Context) []Field {
	fields, ok := ctx.Value(ctxKey{}).([]Field)
	if !ok {
		return nil
	}

	return fields
}
