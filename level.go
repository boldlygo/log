package log

import (
	"encoding"
	"errors"
	"flag"
)

var errUnknownLevel = errors.New("unknown level")

// Level is the log severity level.
type Level int8

// check that Level implements the expected interfaces.
var (
	_ Field                    = Level(0)
	_ encoding.TextMarshaler   = Level(0)
	_ Field                    = (*Level)(nil)
	_ flag.Getter              = (*Level)(nil)
	_ encoding.TextMarshaler   = (*Level)(nil)
	_ encoding.TextUnmarshaler = (*Level)(nil)
)

// Common log severity levels.
const (
	LevelTrace Level = iota - 2
	LevelDebug
	LevelInfo
	LevelWarning
	LevelError
	LevelFatal // Logger implementations may call os.Exit(1).
	LevelPanic // Logger implementations may panic.
)

// Get implements the Field and flag.Getter interfaces.
func (l Level) Get() interface{} { return l }

// Key implements the Field interface.
func (Level) Key() string { return FieldKeyLevel }

// Set implements the flag.Getter interface.
func (l *Level) Set(s string) error { return l.UnmarshalText([]byte(s)) }

// ParseLevel parses s into a Level.
func ParseLevel(s string) (Level, error) {
	lvl, ok := map[string]Level{
		"trace":   LevelTrace,
		"debug":   LevelDebug,
		"info":    LevelInfo,
		"warning": LevelWarning,
		"error":   LevelError,
		"fatal":   LevelFatal,
		"panic":   LevelPanic,
	}[s]

	var err error
	if !ok {
		err = errUnknownLevel
	}

	return lvl, err
}

func (l Level) String() string {
	return map[Level]string{
		LevelTrace:   "trace",
		LevelDebug:   "debug",
		LevelInfo:    "info",
		LevelWarning: "warning",
		LevelError:   "error",
		LevelFatal:   "fatal",
		LevelPanic:   "panic",
	}[l]
}

// MarshalText implements the encoding.TextMarshaler interface.
func (l Level) MarshalText() ([]byte, error) { return []byte(l.String()), nil }

// UnmarshalText implements encoding.TextUnmarshaler.
func (l *Level) UnmarshalText(text []byte) error {
	lvl, err := ParseLevel(string(text))
	if err == nil {
		*l = lvl
	}

	return err
}
