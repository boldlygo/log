// Package log defines the Logger interface and a basic implementation.
package log

import (
	"context"
	"io"
	"io/ioutil"
	"log"
	"time"
)

// Formatter is the interface implemented by types that can format a Log entry.
type Formatter interface {
	Format(entry) ([]byte, error)
}

// Log is a basic leveled, structured log.
type Log struct {
	w        io.Writer
	minLevel Level
	f        Formatter
}

// Check that Log implements Logger.
var _ Logger = Log{}

// New returns a new Log that writes to w.
func New(w io.Writer, lvl Level, f Formatter) Log {
	return Log{
		w:        w,
		minLevel: lvl,
		f:        f,
	}
}

func (l Log) entry(ctx context.Context, fields []Field) entry {
	fields = merge(FromContext(ctx), fields)

	e := entry{
		Log:    &l,
		time:   time.Now(),
		level:  LevelInfo,
		fields: make(map[string]interface{}, len(fields)),
	}

	for _, f := range fields {
		e.setField(f.Key(), f.Get())
	}

	return e
}

// Writer returns a Writer that logs on each call to Write.
func (l Log) Writer(ctx context.Context, fields ...Field) io.Writer {
	if l.w == nil {
		return ioutil.Discard
	}

	return l.entry(ctx, fields)
}

// Log writes a new log entry with any fields stored in ctx and the given field(s).
func (l Log) Log(ctx context.Context, fields ...Field) {
	_, err := l.Writer(ctx, fields...).Write(nil)
	if err == nil {
		return
	}
}

// StdLog returns a Go standard library Logger that writes to the io.Writer from l.Writer(ctx, fields...).
func (l Log) StdLog(ctx context.Context, fields ...Field) *log.Logger {
	return log.New(l.Writer(ctx, fields...), "", 0)
}
